"use strict";

//var bodyElement = document.body;
var divs = $("body div");
divs[0].style.backgroundColor = "pink";

/* 
 //One way to get the last p and make it red:
 var paragraphs = bodyElement.getElementsByTagName("p");
 lastParagraph = paragraphs[paragraphs.length-1];
 lastParagraph.style.color="red";
 */

//Another way, better:
var lastParagraph = $("#footer").css("color", "red"); //Note, getElementById is a method of the document node, not a method of any element nodes!!!
//lastParagraph.style.color = "red";


//divs[1].style.visibility = "hidden";
/*
 // Another way. But unlike setting visibility, this way does not preserve the space allocated to the div (the 'block' display of divs):
 divs[1].style.display="none";
 */

// One way:
// we get back a list of all elements with class=subtitle, even though the list returned only happens to have one item this time.
var subtitles = $(".subtitle");
// get the first subtitle, get its first child (<small>), get its first child which is a textnode and then the latter's value
subtitles[0].firstChild.nodeValue = "Hello World";


var myButton = document.getElementById("makeBold");
//var myButtons = bodyElement.getElementsByTagName("button");
//console.log(myButtons[0]);

// When you click the button, set the style for all paragraphs. Accomplished without looping,
// uses createElement() to create a <style> element. See http://www.w3schools.com/jsref/dom_obj_style.asp

$("#makeBold").click( function (){
    var element = $("<style> p {font-weight: bold} </style>");
    $("head").append(element);
});



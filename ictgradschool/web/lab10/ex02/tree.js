"use script";

function makePageInteractive() {
 //   var container = ;
    var baubles =$("#container").children(".bauble");
    var i = -1;

    for (i = 0; i < baubles.length; i++) {

        baubles[i].onmouseover = function () {

            // Add the "fall" class to the bauble.
            this.classList.add("fall");
        }
    }
}

// Assign an event handler so that the makePageInteractive function is called when the window has loaded.
window.onload = makePageInteractive;